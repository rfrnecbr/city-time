<?php

namespace App\Exceptions;

use Exception;

class CityNotFoundException extends Exception
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json([
            'error' => trans('errors.city_not_found')
        ], 404);
    }
}