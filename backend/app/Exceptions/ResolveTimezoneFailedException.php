<?php

namespace App\Exceptions;

use Exception;

class ResolveTimezoneFailedException extends Exception
{
    /**
     * @inheritdoc
     */
    public function render()
    {
        return response()->json([
            'error' => trans('errors.resolve_timezone_failed')
        ], 404);
    }
}