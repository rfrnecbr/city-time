<?php

namespace App\Repositories;

use App\Models\Address;
use App\Exceptions\CityNotFoundException;
use App\Services\TimezoneResolver;

class CityRepository
{
    /**
     * Возвращает город по его названию
     *
     * @param string $name
     * @return Address
     */
    public function cityByName(string $name): ?Address
    {
        /** @var Address $address */
        $address = Address::query()
            ->where('shortname', '=', 'г')
            ->where('official_name', 'like', "%$name%")
            ->first();

        return $address;
    }

    /**
     * Возвращает город по ОКАТО
     *
     * @param string $okato
     * @return Address
     */
    public function cityByOkato(string $okato): ?Address
    {
        /** @var Address $address */
        $address = Address::query()
            ->where('shortname', 'г')
            ->where('okato', $okato)
            ->first();

        return $address;
    }

    /**
     * Возвращает массив, содержащий название города и его текущее время
     *
     * @param string $query
     * @return array
     * @throws CityNotFoundException
     * @throws \App\Exceptions\ResolveTimezoneFailedException
     */
    public function getCityTime(string $query): array
    {
        $city = $this->resolveCity($query);

        if (cache()->has('time' . $city->okato))
        {
            return cache()->get('time' . $city->okato);
        }
        else
        {
            $timezoneResolver = new TimezoneResolver();
            $timezone = $timezoneResolver->resolve($city->official_name);

            $time = $this->getTime($timezone);

            $result = [
                'name' => $city->official_name,
                'time' => $time
            ];

            cache()->put('time' . $city->okato, $result, env('CACHE_TIME'));

            return $result;
        }
    }

    /**
     * Проверяет, является ли строка ОКАТО
     * @param string $query
     * @return bool
     */
    protected function isOkato(string $query): bool
    {
        return preg_match('/[0-9]+/', $query) === 1;
    }

    /**
     * Возвращает город по названию или коду ОКАТО в зависимости от того, что было передано
     *
     * @param string $query
     * @return Address
     * @throws CityNotFoundException
     */
    protected function resolveCity(string $query): Address
    {
        if ($this->isOkato($query))
        {
            $city = $this->cityByOkato($query);
        }
        else
        {
            $city = $this->cityByName($query);
        }

        if (is_null($city))
        {
            throw new CityNotFoundException();
        }

        return $city;
    }

    /**
     * Возвращает текущую дату и время с учетом часового пояса
     *
     * @param string $timezone
     * @return string
     */
    protected function getTime(string $timezone): string
    {
        $date = new \DateTime();
        $timezone = new \DateTimeZone($timezone);
        $date->setTimezone($timezone);
        return $date->format('Y-m-d H:i');
    }
}
