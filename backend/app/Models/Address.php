<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Address
 * @package App\Models
 *
 * @property int $id
 * @property string $shortname
 * @property string $official_name
 * @property string $okato
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Address extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'shortname', 'official_name', 'okato',
    ];
}
