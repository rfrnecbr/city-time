<?php

namespace App\Services\Dadata;

use App\Services\TimezoneInterface;
use Illuminate\Support\Facades\Http;

class Timezone implements TimezoneInterface
{
    /**
     * @var string
     */
    protected $baseUrl = 'https://dadata.ru';

    /**
     * @var string
     */
    protected $queryUrl = '/api/v2/clean/address';

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $secret;

    /**
     * Timezone constructor.
     *
     * @param string $token
     * @param string $secret
     */
    public function __construct(string $token, string $secret)
    {
        $this->token = $token;
        $this->secret = $secret;
    }

    /**
     * @inheritdoc
     */
    public function resolve(string $city): ?string
    {
        $result = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Token ' . $this->token,
            'X-Secret' => $this->secret
        ])
            ->post("{$this->baseUrl}{$this->queryUrl}", [$city])
            ->json();

        if (
            !is_null($result)
            && count($result) > 0
            && isset($result[0]['timezone'])
            && !is_null($result[0]['timezone'])
        )
        {
            return str_replace('UTC', '', $result[0]['timezone']);
        }

        return null;
    }
}
