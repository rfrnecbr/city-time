<?php

namespace App\Services;

use App\Exceptions\ResolveTimezoneFailedException;

class TimezoneResolver
{
    /**
     * Возвращает часовой пояс по названию города
     *
     * @param string $city
     * @return string
     * @throws ResolveTimezoneFailedException
     */
    public function resolve(string $city): string
    {
        $i = 0;
        $timezone = null;

        $resolvers = config('services.timezone-resolvers');

        if (count($resolvers) > 0)
        {
            do
            {
                /** @var TimezoneInterface $resolver */
                $resolver = app($resolvers[$i]);
                $timezone = $resolver->resolve($city);
                $i++;
            }
            while (is_null($timezone) && $i < count($resolvers));
        }

        if (is_null($timezone))
        {
            throw new ResolveTimezoneFailedException();
        }

        return $timezone;
    }
}
