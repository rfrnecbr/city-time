<?php

namespace App\Services\HTMLWeb;

use App\Services\GeoDecoder\GeoDecoderTrait;
use App\Services\TimezoneInterface;
use Illuminate\Support\Facades\Http;

class Timezone implements TimezoneInterface
{
    use GeoDecoderTrait;

    /**
     * @var string
     */
    protected $baseUrl = 'https://htmlweb.ru';

    /**
     * @var string
     */
    protected $token;

    /**
     * Timezone constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @inheritdoc
     */
    public function resolve(string $city): ?string
    {
        $coordinates = $this->decode($city);

        if (!is_null($coordinates))
        {
            $response = Http::get("{$this->baseUrl}/geo/api.php?timezone={$coordinates[1]},{$coordinates[0]}&json&api_key={$this->token}");
            $result = $response->json();

            if (isset($result['name']))
            {
                return $result['name'];
            }
        }

        return null;
    }
}
