<?php

namespace App\Services;

interface TimezoneInterface
{
    /**
     * Возвращает часовой пояс по названию города
     *
     * @param string $city
     * @return null|string
     */
    public function resolve(string $city): ?string;
}