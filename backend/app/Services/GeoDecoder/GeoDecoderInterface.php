<?php

namespace App\Services\GeoDecoder;

interface GeoDecoderInterface
{
    /**
     * Возвращает массив координат по названию города
     *
     * @param string $name
     * @return integer[]|null
     */
    public function decode(string $name): ?array;
}
