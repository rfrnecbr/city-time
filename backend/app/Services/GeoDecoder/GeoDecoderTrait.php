<?php

namespace App\Services\GeoDecoder;

trait GeoDecoderTrait
{
    /**
     * @param $city
     * @return integer[]|null
     */
    protected function decode($city): ?array
    {
        /** @var GeoDecoderInterface $decoder */
        $decoder = app(GeoDecoderInterface::class);
        return $decoder->decode($city);
    }
}
