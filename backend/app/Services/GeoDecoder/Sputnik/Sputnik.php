<?php

namespace App\Services\GeoDecoder\Sputnik;

use App\Services\GeoDecoder\GeoDecoderInterface;
use Illuminate\Support\Facades\Http;

class Sputnik implements GeoDecoderInterface
{
    /**
     * @inheritDoc
     */
    public function decode(string $name): ?array
    {
        try {
            $response = Http::get("http://search.maps.sputnik.ru/search/addr?q=$name");
            return $response->json()['result']['address'][0]['features'][0]['geometry']['geometries'][0]['coordinates'];
        } catch (\Exception $exception) {
            return null;
        }
    }
}
