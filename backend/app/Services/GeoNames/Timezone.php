<?php

namespace App\Services\GeoNames;

use App\Services\GeoDecoder\GeoDecoderTrait;
use App\Services\TimezoneInterface;
use Illuminate\Support\Facades\Http;

class Timezone implements TimezoneInterface
{
    use GeoDecoderTrait;

    /**
     * @var string
     */
    protected $baseUrl = 'http://api.geonames.org';

    /**
     * Имя пользователя для выполнения запроса
     *
     * @var string
     */
    protected $username;

    /**
     * Timezone constructor.
     *
     * @param string $username
     */
    public function __construct(string $username)
    {
        $this->username = $username;
    }

    /**
     * @inheritdoc
     */
    public function resolve(string $city): ?string
    {
        $coordinates = $this->decode($city);

        if (!is_null($coordinates))
        {
            $response = Http::get("{$this->baseUrl}/timezoneJSON?lat={$coordinates[1]}&lng={$coordinates[0]}&username={$this->username}");
            $result = $response->json();

            if (isset($result['timezoneId']))
            {
                return $result['timezoneId'];
            }
        }

        return null;
    }
}
