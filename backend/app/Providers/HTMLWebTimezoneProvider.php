<?php

namespace App\Providers;

use App\Services\HTMLWeb\Timezone;
use Illuminate\Support\ServiceProvider;

class HTMLWebTimezoneProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->app->bind(Timezone::class, function () {
            return new Timezone(config('services.html_web.token'));
        });
    }
}
