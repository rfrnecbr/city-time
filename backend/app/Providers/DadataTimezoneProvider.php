<?php

namespace App\Providers;

use App\Services\Dadata\Timezone;
use Illuminate\Support\ServiceProvider;

class DadataTimezoneProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->app->bind(Timezone::class, function () {
            return new Timezone(config('services.dadata.token'), config('services.dadata.secret'));
        });
    }
}
