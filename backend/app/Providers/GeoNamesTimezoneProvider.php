<?php

namespace App\Providers;

use App\Services\GeoNames\Timezone;
use Illuminate\Support\ServiceProvider;

class GeoNamesTimezoneProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->app->bind(Timezone::class, function () {
            return new Timezone(config('services.geonames.username'));
        });
    }
}
