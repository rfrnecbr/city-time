<?php

namespace App\Providers;

use App\Services\GeoDecoder\GeoDecoderInterface;
use App\Services\GeoDecoder\Sputnik\Sputnik;
use Illuminate\Support\ServiceProvider;

class GeoDecoderProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->app->bind(GeoDecoderInterface::class, function () {
            return new Sputnik();
        });
    }
}
