<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ResolveTimeRequest;
use App\Repositories\CityRepository;

class TimeController extends Controller
{
    /**
     * Возвращает город и текущее время в нем
     *
     * @param ResolveTimeRequest $request
     * @param CityRepository $repository
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CityNotFoundException
     * @throws \App\Exceptions\ResolveTimezoneFailedException
     */
    public function resolveTime(ResolveTimeRequest $request, CityRepository $repository)
    {
        $city = $repository->getCityTime($request->input('city'));
        return response()->json(compact('city'));
    }
}
