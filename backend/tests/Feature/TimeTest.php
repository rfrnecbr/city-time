<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TimeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var bool
     */
    protected $seed = true;

    /**
     * Успешный запрос на получение времени
     */
    public function testSuccessRequest()
    {
        $response = $this->get('/api/time?city=Ульяновск');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'city' => [
                'name', 'time',
            ],
        ]);
        $response->assertExactJson([
            'city' => [
                'name' => 'Ульяновск',
                'time' => Carbon::now('Europe/Samara')->format('Y-m-d H:i'),
            ],
        ]);
    }

    /**
     * Тест на ошибки валидации
     */
    public function testValidationFailedRequest()
    {
        $response = $this
            ->withHeader('Accept', 'application/json')
            ->get('/api/time');

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'city'
            ]
        ]);
    }

    /**
     * Тест на запрос города, отсутствующего в базе
     */
    public function testCityNotFoundRequest()
    {
        $response = $this
            ->withHeader('Accept', 'application/json')
            ->get('/api/time?city=NotFound');

        $response->assertStatus(404);
        $response->assertJsonStructure([
            'error',
        ]);
    }

    /**
     * Тест на недоступность сервисов определения часового пояса
     */
    public function testResolveTimezoneFailedRequest()
    {
        config([
            'services.timezone-resolvers' => [],
        ]);

        $response = $this
            ->withHeader('Accept', 'application/json')
            ->get('/api/time?city=Ульяновск');

        $response->assertStatus(404);
        $response->assertJsonStructure([
            'error',
        ]);
    }
}
