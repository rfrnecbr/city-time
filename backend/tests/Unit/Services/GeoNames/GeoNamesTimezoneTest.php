<?php

namespace Tests\Unit\Services\GeoNames;

use App\Services\GeoNames\Timezone;
use App\Services\TimezoneInterface;
use Tests\TestCase;

class GeoNamesTimezoneTest extends TestCase
{
    /**
     * Проверка на корректность определения часового пояса
     */
    public function testResolve()
    {
        /** @var TimezoneInterface $timezone */
        $timezone = app(Timezone::class);
        $this->assertEquals('Asia/Yekaterinburg', $timezone->resolve('Екатеринбург'));
        $this->assertEquals('Europe/Ulyanovsk', $timezone->resolve('Ульяновск'));
        $this->assertEquals('Europe/Moscow', $timezone->resolve('Москва'));
    }

    /**
     * Запрос несуществующего города
     */
    public function testFail()
    {
        /** @var TimezoneInterface $timezone */
        $timezone = app(Timezone::class);
        $this->assertEquals(null, $timezone->resolve('NotFound'));
    }
}
