<?php

namespace Tests\Unit\Services\Dadata;

use App\Services\Dadata\Timezone;
use Tests\TestCase;

class DadataTimezoneTest extends TestCase
{
    /**
     * Проверка на корректность определения часового пояса
     */
    public function testResolve()
    {
        /** @var Timezone $timezone */
        $timezone = app(Timezone::class);
        $this->assertEquals('+5', $timezone->resolve('Екатеринбург'));
        $this->assertEquals('+4', $timezone->resolve('Ульяновск'));
        $this->assertEquals('+3', $timezone->resolve('Москва'));
    }

    /**
     * Запрос несуществующего города
     */
    public function testFail()
    {
        /** @var Timezone $timezone */
        $timezone = app(Timezone::class);
        $this->assertEquals(null, $timezone->resolve('NotFound'));
    }
}
