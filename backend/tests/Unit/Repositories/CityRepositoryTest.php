<?php

namespace Tests\Unit\Repositories;

use App\Exceptions\CityNotFoundException;
use App\Exceptions\ResolveTimezoneFailedException;
use App\Repositories\CityRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CityRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var bool
     */
    protected $seed = true;

    /**
     * Тест на корректное определение часового пояса
     *
     * @throws CityNotFoundException
     * @throws ResolveTimezoneFailedException
     */
    public function testResolveCity()
    {
        $repository = new CityRepository();
        $result = $repository->getCityTime('Ульяновск');

        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('time', $result);

        $this->assertEquals('Ульяновск', $result['name']);
        $this->assertEquals(Carbon::now('Europe/Samara')->format('Y-m-d H:i'), $result['time']);
    }

    /**
     * Тест на запрос города, которого нет в базе
     */
    public function testCityNotFound()
    {
        $repository = new CityRepository();

        try {
            $repository->getCityTime('NotFound');
        } catch (\Exception $exception) {
            $this->assertInstanceOf(CityNotFoundException::class, $exception);
        }
    }

    /**
     * Тест на проверку недоступности сервисов определения часового пояса
     */
    public function testResolveFailed()
    {
        $repository = new CityRepository();

        config([
            'services.timezone-resolvers' => [],
        ]);

        try {
            $repository->getCityTime('Ульяновск');
        } catch (\Exception $exception) {
            $this->assertInstanceOf(ResolveTimezoneFailedException::class, $exception);
        }
    }
}
