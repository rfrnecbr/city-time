<?php

return [
    'city_not_found' => 'Город не найден',
    'resolve_timezone_failed' => 'Не удалось определить часовой пояс',
];