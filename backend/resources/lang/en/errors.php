<?php

return [
    'city_not_found' => 'City not found',
    'resolve_timezone_failed' => 'Resolve timezone failed',
];