<?php

use Illuminate\Support\Facades\Route;

Route::get('time', 'TimeController@resolveTime');
