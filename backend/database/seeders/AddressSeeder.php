<?php

namespace Database\Seeders;

use App\Models\Address;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    protected $addresses = [
        [
            'shortname' => 'г',
            'official_name' => 'Ульяновск',
            'okato' => '73401000000',
        ],
        [
            'shortname' => 'г',
            'official_name' => 'Москва',
            'okato' => '45000000000',
        ],
        [
            'shortname' => 'г',
            'official_name' => 'Калининград',
            'okato' => '27401000000',
        ],
        [
            'shortname' => 'г',
            'official_name' => 'Екатеринбург',
            'okato' => '65401000000',
        ],
        [
            'shortname' => 'г',
            'official_name' => 'Хабаровск',
            'okato' => '08401000000',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->addresses as $address)
        {
            Address::create($address);
        }
    }
}
