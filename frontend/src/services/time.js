import axios from './axios';

export default class Time {
  static resolve({ city }) {
    return axios.get(`/time?city=${city}`)
      .then(({ data }) => data.city);
  }
}
